package com.example.yeasin.dialogflowrnd;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by yeasin on 04/10/18.
 */
public class FirebaseApp extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
